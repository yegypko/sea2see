/* jshint node:true */

'use strict';



const gulp = require('gulp');

const browserSync = require('browser-sync');

const sass = require('gulp-sass');

const autoprefixer = require('gulp-autoprefixer');

const concat = require('gulp-concat');

const uglify = require('gulp-uglify');

const stripCssComments = require('gulp-strip-css-comments');

gulp.task('server', function() {

  browserSync({
      server: {
          baseDir: "assets"
      }
  });

});


gulp.task('scss', (done) => {

  gulp.src(['assets/scss/main.scss']).

      pipe(sass({

        outputStyle: 'compressed',

      }).on('error', sass.logError)).

      pipe(stripCssComments()).

      pipe(autoprefixer()).

      pipe(concat('style.css')).

      pipe(gulp.dest('assets/css')).

      pipe(browserSync.stream());

  done();

});

gulp.task('watch', function() {
  gulp.watch("assets/scss/**/*.scss", gulp.parallel('scss'));
  gulp.watch("assets/*.html").on('change', browserSync.reload);
  
});


gulp.task('scripts', (done) => {

  gulp.src(

      [

        'assets/js/src/*.js',

      ]).

      pipe(concat('script.js')).

      pipe(uglify()).

      pipe(gulp.dest('assets/js')).

      pipe(browserSync.stream());

  done();

});







gulp.task('default', gulp.parallel('watch', 'server', 'scss', 'scripts'));